# Bluetooth Detection

ESP32 Application that uses BLE Module to detect bluetooth devices.

The app starts in scanning state. In this state it scans for devices that advertise their connection. Once a device is detected a LED light turns on with different levels of intensity.
When a button is pressed for longer than 3 seconds it goes in the advertise state so that other devices can discover it.



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Hardware:
- An ESP32 board
- USB cable - USB A / micro USB B
- An ESP32 board
- USB cable - USB A / micro USB B
- Computer running Linux, or macOS

Software:
- Toolchain to compile code for ESP32
- Build tools - CMake and Ninja to build a full Application for ESP32
- ESP-IDF that essentially contains API (software libraries and source code) for ESP32 and scripts to operate the Toolchain

### Installing


- Install prerequisites:
    - sudo apt-get install git wget flex bison gperf python python-pip python-setuptools cmake ninja-build ccache libffi-dev libssl-dev
Get ESP-IDF:
    - cd ~/esp
    - git clone --recursive https://github.com/espressif/esp-idf.git
- Set up the tools:
    - cd ~/esp/esp-idf
    - ./install.sh
- Set up the environment variables:
    - . $HOME/esp/esp-idf/export.sh

### Running a project 
- Connect your device and check under what serial port the board is visible:
    - ls /dev/tty*
- Configure the development board:
    - cd ~/esp/project_name
    - idf.py set-target esp32
    - idf.py menuconfig
- Build the project:
    - idf.py build

## Deployment
Flash onto device:
- idf.py -p PORT [-b BAUD] flash
	Note:
	- PORT is your ESP32 board's serial port name; 
	- BAUD is the baud rate (default is 460800).

## Built With

* [Eclipse](https://www.eclipse.org/) - The IDE used

## Versioning

We use [gitlab](https://gitlab.com) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/atr-repo/practica-2020). 


## Authors
